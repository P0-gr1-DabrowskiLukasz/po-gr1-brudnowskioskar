package pl.edu.uwm.wmii.brudnowskioskar.laboratorium00;

public class Zadanie4
{
    public static void main(String[] args) {
        double saldo = 1000;
        double pierwszy;
        double drugi;
        double trzeci;

        pierwszy = saldo * 1.06;
        drugi = pierwszy * 1.06;
        trzeci = drugi * 1.06;

        System.out.println("Po pierwszym roku: " + pierwszy);
        System.out.println("Po drugim roku: " + drugi);
        System.out.println("Po trzecim roku: " + trzeci);
    }
}
