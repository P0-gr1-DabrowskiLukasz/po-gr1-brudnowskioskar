package pl.edu.uwm.wmii.brudnowskioskar.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1g {
    public static void main(String[] args) {
        Scanner read = new Scanner(System.in);
        System.out.println("Podaj liczbe calkowita z przedzialu <1-100> :");
        int n = read.nextInt();
        while (n > 100 || n < 1) {
            System.out.println("Niepoprawny przedzial, podaj jeszcze raz:");
            n = read.nextInt();
        }
        Random rnd = new Random();
        int liczby[] = new int[n];

        int lewy, prawy;
        System.out.println("Lewy: ");
        lewy=read.nextInt();
        System.out.println("Prawy: ");
        prawy=read.nextInt();

        for (int i = 0; i < n; i++) {
            liczby[i] = rnd.nextInt(1999) - 999;
        }

        for(int i = 0; i < n; i++){
            System.out.println(liczby[i]);
        }
        int a;
        int b=0;
        for(int i=lewy-1; i<prawy-1; i++){
            a=liczby[i];
            liczby[i]=liczby[prawy-1-b];
            liczby[prawy-1-b]=a;

            if(i==(prawy-1-b))
                break;
            b++;
        }
        for(int i=0; i<n; i++){
            System.out.println(liczby[i]);
        }
    }
}
