package pl.edu.uwm.wmii.brudnowskioskar.laboratorium02;

import java.util.Scanner;
import java.util.Random;

public class Zadanie2c
{
    public static void main(String[] args)
    {
        Scanner read = new Scanner(System.in);
        System.out.println("Podaj liczbe calkowita z przedzialu <1-100> :");
        int n = read.nextInt();
        while(n>100 || n<1)
        {
            System.out.println("Niepoprawny przedzial, podaj jeszcze raz:");
            n = read.nextInt();
        }
        int liczby[] = new int[n];
        generuj(liczby, n, -999, 999);
        wypisz(liczby);
        int m=ileMaksymalnych(liczby);
        System.out.println(m);
    }

    public static void generuj(int tab[], int n, int min, int max)
    {
        Random rnd = new Random();
        int x;
        if (min>=0)
            x=max-min+1;
        else
            x=(-1)*min+max+1;

        for (int i=0; i<n; i++)
            tab[i]=rnd.nextInt(x)+min;
    }

    public static void wypisz(int tab[])
    {
        for (int i=0; i<tab.length; i++)
        {
            System.out.print(tab[i]+"  ");
        }
        System.out.println(" ");
    }

    public static int ileMaksymalnych (int tab[])
    {
        int max=tab[0];
        int x=0;
        for (int i = 0; i < tab.length; i++)
        {
            if(tab[i] > max)
            {
                max = tab[i];
                x=0;
            }
            if(tab[i] == max)
                x++;
        }
        System.out.println("Najwiekszy element w tablicy: "+max);
        System.out.print("Ilosc wystapień: ");
        return x;
    }
}
