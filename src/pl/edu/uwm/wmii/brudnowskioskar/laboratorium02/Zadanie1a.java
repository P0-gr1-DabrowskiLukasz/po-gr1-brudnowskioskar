package pl.edu.uwm.wmii.brudnowskioskar.laboratorium02;

import java.util.Scanner;
import java.util.Random;

public class Zadanie1a
{
    public static void main(String[] args) {
        Scanner read = new Scanner(System.in);
        System.out.println("Podaj liczbe calkowita z przedzialu <1-100> :");
        int n = read.nextInt();
        while(n>100 || n<1)
        {
            System.out.println("Niepoprawny przedzial, podaj jeszcze raz:");
            n = read.nextInt();
        }
        Random rnd = new Random();
        int liczby[] = new int[n];
        int p=0;
        int np=0;
        for(int i=0; i<n; i++)
        {
            liczby[i]=rnd.nextInt(1999)-999;
            System.out.println(liczby[i]);
            if (liczby[i]%2==0)
                p++;
            else
                np++;
        }
        System.out.println("Parzyste: " + p + "  Nieparzyste: "+ np);
    }
}